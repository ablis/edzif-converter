# EDZIF converter

[![pipeline status](https://gitlab.com/ablis/edzif-converter/badges/master/pipeline.svg)](https://gitlab.com/ablis/edzif-converter/commits/master)
[![coverage report](https://gitlab.com/ablis/edzif-converter/badges/master/coverage.svg)](https://gitlab.com/ablis/edzif-converter/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat)](https://github.com/prettier/prettier)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg?style=flat)](https://conventionalcommits.org)

Library to format DNS zones in EDZIF format into formats usable with other systems.

## Development

EDZIF converter is written in [TypeScript][]. To compile TypeScript to JavaScript, run:

    npm run build

To compile TypeScript to JavaScript automatically when the code changes, run:

    npm run start

To run the test suite, run:

    npm run test

[typescript]: https://www.typescriptlang.org/

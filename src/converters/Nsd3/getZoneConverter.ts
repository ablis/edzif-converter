import { GenericZoneFileRecordConverterOptions } from '../GenericZoneFile/GenericZoneFileRecordConverter'
import { Nsd3RecordConverter } from './Nsd3RecordConverter'
import { Nsd3ZoneConverter } from './Nsd3ZoneConverter'

/**
 * Return a zone converter instance with default configuration.
 */
export const getZoneConverter = (
  recordOptions?: GenericZoneFileRecordConverterOptions
) => {
  const recordConverter = new Nsd3RecordConverter(recordOptions)
  return new Nsd3ZoneConverter(recordConverter)
}

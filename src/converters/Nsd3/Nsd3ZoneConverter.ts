import { EdzifZone } from 'edzif-validator'
import { GenericZoneFileZoneConverter } from '../GenericZoneFile/GenericZoneFileZoneConverter'

export class Nsd3ZoneConverter extends GenericZoneFileZoneConverter {
  /**
   * Generate zone file.
   *
   * @param {object} zone
   *
   * @return {string}
   */
  generate(zone: EdzifZone) {
    const header = `
;## FORWARD Zone -

$ORIGIN ${zone.name}.
$TTL 86400
`
    return [header, super.generate(zone)].join('\n')
  }

  /**
   * Generate config file fragment for NSD3.
   *
   * @param {array} zones
   *   Zone objects.
   *
   * @return {string}
   *   Configuration fragment.
   */
  generateConfiguration(zones: EdzifZone[]) {
    const fragments = []

    for (let zone of zones) {
      const fileName = this.generateFileName(zone)

      // Weird indentation to avoid extraneous whitespace in output, sorry.
      fragments.push(`
zone:
    name: "${zone.name}."
    zonefile: "${fileName}"
`)
    }

    return fragments.join('')
  }
}

import { EdzifZone } from 'edzif-validator'
import { GenericZoneFileRecordConverter } from './GenericZoneFileRecordConverter'

interface GenerateMultipleOutput {
  [propType: string]: string
}

export class GenericZoneFileZoneConverter {
  recordConverter: GenericZoneFileRecordConverter

  constructor(recordConverter: GenericZoneFileRecordConverter) {
    this.recordConverter = recordConverter
  }

  /**
   * Generate zone file.
   *
   * @param {object} zone
   *
   * @return {string}
   */
  generate(zone: EdzifZone): string {
    const contents = []

    if (zone.records) {
      for (let record of zone.records) {
        contents.push(this.recordConverter.recordTypeDispatch(record))
      }
    }

    let output = contents.join('\n')

    // If we have output, be sure to end the file with a linebreak.
    if (output) {
      output += '\n'
    }

    return output
  }

  generateFileName(zone: EdzifZone): string {
    return `${zone.name}.zone`
  }

  generateMultiple(zones: EdzifZone[]): GenerateMultipleOutput {
    const output: GenerateMultipleOutput = {}

    for (let zone of zones) {
      output[zone.name] = this.generate(zone)
    }

    return output
  }
}

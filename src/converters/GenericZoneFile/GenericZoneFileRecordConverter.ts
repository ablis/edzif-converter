import { EdzifRecord } from 'edzif-validator'

type OptionalPrefix = string | null | undefined
type OptionalTtl = number | null | undefined

export interface GenericZoneFileRecordConverterOptions {
  absolutifyHostnames?: boolean
}

const defaultOptions = {
  absolutifyHostnames: false
}

// Check if a hostname is absolute.
// Absolute hostnames end in @ or .
const absoluteHostNameTester = /[@.]$/

export class GenericZoneFileRecordConverter {
  options: GenericZoneFileRecordConverterOptions

  constructor(options: GenericZoneFileRecordConverterOptions = {}) {
    this.options = Object.assign({}, defaultOptions, options)
  }

  A(prefix: OptionalPrefix, ttl: OptionalTtl, address: string) {
    return `${this.formatPrefix(prefix, ttl)} IN A ${address}`
  }

  AAAA(prefix: OptionalPrefix, ttl: OptionalTtl, address: string) {
    return `${this.formatPrefix(prefix, ttl)} IN AAAA ${address}`
  }

  CNAME(prefix: OptionalPrefix, ttl: OptionalTtl, hostname: string) {
    return `${this.formatPrefix(prefix, ttl)} CNAME ${this.hostname(hostname)}`
  }

  MX(
    prefix: OptionalPrefix,
    ttl: OptionalTtl,
    priority: number,
    hostname: string
  ) {
    return `${this.formatPrefix(prefix, ttl)} MX ${priority} ${this.hostname(
      hostname
    )}`
  }

  NS(prefix: OptionalPrefix, ttl: OptionalTtl, hostname: string) {
    return `${this.formatPrefix(prefix, ttl)} IN NS ${this.hostname(hostname)}`
  }

  SOA(
    prefix: OptionalPrefix,
    primaryServer: string,
    responsiblePerson: string,
    serial: number,
    refresh: number,
    retry: number,
    expire: number,
    minimumTtl: number
  ) {
    return `${this.formatPrefix(prefix)} IN SOA ${this.hostname(
      primaryServer
    )} ${this.hostname(
      responsiblePerson
    )} (${serial} ${refresh} ${retry} ${expire} ${minimumTtl})`
  }

  SRV(
    prefix: OptionalPrefix,
    ttl: OptionalTtl,
    weight: number,
    priority: number,
    port: number,
    hostname: string
  ) {
    return `${this.formatPrefix(
      prefix,
      ttl
    )} IN SRV ${weight} ${priority} ${port} ${this.hostname(hostname)}`
  }

  TXT(prefix: OptionalPrefix, ttl: OptionalTtl, content: string) {
    return `${this.formatPrefix(prefix, ttl)} IN TXT (${this.formatTXTContent(
      content
    )})`
  }

  formatPrefix(prefix: OptionalPrefix, ttl?: OptionalTtl) {
    const output = []

    if (typeof prefix === 'string' && prefix.length > 0) {
      output.push(prefix)
    } else {
      output.push('@')
    }

    if (typeof ttl === 'number' && ttl >= 0) {
      output.push(ttl)
    }

    return output.join(' ')
  }

  formatTXTContent(content: string) {
    // Remove all characters that are not printable ASCII.
    let escapedText = content.replace(/[^\x20-\x7E]+/g, '')

    // Escape backslashes.
    escapedText = escapedText.replace(/\\/g, '\\\\')

    // Escape backticks.
    escapedText = escapedText.replace(/`/g, '``')

    // Escape quotes.
    escapedText = escapedText.replace(/"/g, '\\"')

    // Now we need chunking, since there's a 255 character limit on TXT field values.
    // See http://serverfault.com/a/255676/766
    const chunks = escapedText.match(/.{1,250}/g)

    if (chunks && chunks.length > 0) {
      return '"' + chunks.join('" "') + '"'
    }

    return ''
  }

  hostname(hostname?: string) {
    // Make sure we have a string, even if it's empty.
    let output = hostname ? hostname + '' : ''

    // Trim off any spaces.
    output = output.trim()

    if (this.options.absolutifyHostnames) {
      // If hostname is not absolute, add a . at the end to make so.
      if (!absoluteHostNameTester.test(output)) {
        output += '.'
      }
    }

    return output
  }

  recordTypeDispatch(record: EdzifRecord) {
    if (record.record_type === 'A') {
      return this.A(record.prefix, record.ttl, record.address)
    } else if (record.record_type === 'AAAA') {
      return this.AAAA(record.prefix, record.ttl, record.address)
    } else if (record.record_type === 'CNAME') {
      return this.CNAME(record.prefix, record.ttl, record.name)
    } else if (record.record_type === 'MX') {
      return this.MX(record.prefix, record.ttl, record.priority, record.name)
    } else if (record.record_type === 'NS') {
      return this.NS(record.prefix, record.ttl, record.name)
    } else if (record.record_type === 'SOA') {
      return this.SOA(
        record.prefix,
        record.primary_server,
        record.responsible_person,
        record.serial,
        record.refresh,
        record.retry,
        record.expire,
        record.minimum_ttl
      )
    } else if (record.record_type === 'SRV') {
      return this.SRV(
        record.prefix,
        record.ttl,
        record.weight,
        record.priority,
        record.port,
        record.name
      )
    } else if (record.record_type === 'TXT') {
      return this.TXT(record.prefix, record.ttl, record.text_content)
    } else {
      throw new Error(
        'EDZIFCONVERR-001: No renderer found for record' +
          JSON.stringify(record)
      )
    }
  }
}

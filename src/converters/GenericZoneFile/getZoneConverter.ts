import {
  GenericZoneFileRecordConverter,
  GenericZoneFileRecordConverterOptions
} from './GenericZoneFileRecordConverter'
import { GenericZoneFileZoneConverter } from './GenericZoneFileZoneConverter'

/**
 * Return a zone converter instance with default configuration.
 */
export const getZoneConverter = (
  recordOptions?: GenericZoneFileRecordConverterOptions
) => {
  const recordConverter = new GenericZoneFileRecordConverter(recordOptions)
  return new GenericZoneFileZoneConverter(recordConverter)
}

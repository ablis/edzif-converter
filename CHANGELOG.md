# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0-alpha.0](https://gitlab.com/ablis/edzif-converter/compare/v1.0.1...v2.0.0-alpha.0) (2019-05-26)

### Features

- convert all code to TypeScript ([0ff869c](https://gitlab.com/ablis/edzif-converter/commit/0ff869c))

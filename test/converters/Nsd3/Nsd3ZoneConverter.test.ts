import { getZoneConverter } from '../../../src/converters/Nsd3/getZoneConverter'

describe('NSD3 zone conversion', () => {
  const converter = getZoneConverter()

  test('Full valid zone', done => {
    const validZone = require('../../fixtures/zone_full_valid')
    const output = converter.generate(validZone)

    expect(typeof output).toBe('string')
    expect(output).toMatch('43200 IN A 10.0.0.1\n')
    expect(output).toMatch(
      '_submission._tcp 43200 IN SRV undefined 10 587 mail.example.com\n'
    )
    expect(output).toMatch('_dmarc 43200 IN TXT ("v=DMARC1; p=reject")')
    done()
  })

  test('Multiple valid zones', done => {
    const zones = require('../../fixtures/multiple_valid_zones')
    const output = converter.generateMultiple(zones)

    expect(Object.keys(output)).toHaveLength(3)
    expect(output['example.com']).toMatch('www 43200 CNAME example.com\n')
    expect(output['example.net']).toMatch('300 IN A 192.168.1.38\n')
    done()
  })

  test('nsd.conf fragment generation', done => {
    const zones = require('../../fixtures/multiple_valid_zones')
    const output = converter.generateConfiguration(zones)

    expect(typeof output).toBe('string')
    expect(output).toMatch(
      /zone:\n\s+name: "example.net."\n\s+zonefile: "example.net.zone"/
    )
    done()
  })
})

import { RecordTypeTXT } from 'edzif-validator'
import { GenericZoneFileRecordConverter } from '../../../src/index'

describe('GenericZoneFile records conversion', () => {
  const converter = new GenericZoneFileRecordConverter()

  test('A record (root)', done => {
    const output = converter.A('@', 8888, '192.168.1.12')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 8888 IN A 192.168.1.12')
    done()
  })

  test('A record (localhost)', done => {
    const output = converter.A('localhost', 43200, '127.0.0.1')

    expect(typeof output).toBe('string')
    expect(output).toBe('localhost 43200 IN A 127.0.0.1')
    done()
  })

  test('A record (no TTL)', done => {
    const output = converter.A('evil', null, '8.8.8.8')

    expect(typeof output).toBe('string')
    expect(output).toBe('evil IN A 8.8.8.8')
    done()
  })

  test('A record (no prefix)', done => {
    const output = converter.A(null, 1337, '192.168.1.1')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 1337 IN A 192.168.1.1')
    done()
  })

  test('AAAA record (root)', done => {
    const output = converter.AAAA(
      '@',
      3604,
      '2001:0db8:0000:0000:0000:ff00:0042:8329'
    )

    expect(typeof output).toBe('string')
    expect(output).toBe(
      '@ 3604 IN AAAA 2001:0db8:0000:0000:0000:ff00:0042:8329'
    )
    done()
  })

  test('AAAA record (localhost)', done => {
    const output = converter.AAAA('localhost', 43200, '2001:db8::ff00:42:8329')

    expect(typeof output).toBe('string')
    expect(output).toBe('localhost 43200 IN AAAA 2001:db8::ff00:42:8329')
    done()
  })

  test('CNAME record (www)', done => {
    const output = converter.CNAME('www', 43200, 'example.net')

    expect(typeof output).toBe('string')
    expect(output).toBe('www 43200 CNAME example.net')
    done()
  })

  test('CNAME record (*)', done => {
    const output = converter.CNAME('*', null, 'example.net')

    expect(typeof output).toBe('string')
    expect(output).toBe('* CNAME example.net')
    done()
  })

  test('CNAME record (@)', done => {
    const output = converter.CNAME('self', 7200, '@')

    expect(typeof output).toBe('string')
    expect(output).toBe('self 7200 CNAME @')
    done()
  })

  test('MX record (mail)', done => {
    const output = converter.MX('@', 43200, 10, 'smtp.example.net')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 43200 MX 10 smtp.example.net')
    done()
  })

  test('NS record (ns1)', done => {
    const output = converter.NS('@', 19999, 'ns1.example.net')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 19999 IN NS ns1.example.net')
    done()
  })

  test('SOA record (example)', done => {
    const output = converter.SOA(
      null,
      'ns1.example.net',
      'admin.example.net',
      1486414023,
      3600,
      7200,
      864000,
      3600
    )

    expect(typeof output).toBe('string')
    expect(output).toBe(
      '@ IN SOA ns1.example.net admin.example.net (1486414023 3600 7200 864000 3600)'
    )
    done()
  })

  test('SRV record (imap)', done => {
    const output = converter.SRV(
      '_imap._tcp.example.net.',
      86400,
      1,
      10,
      143,
      'imap.example.net.'
    )

    expect(typeof output).toBe('string')
    expect(output).toBe(
      '_imap._tcp.example.net. 86400 IN SRV 1 10 143 imap.example.net.'
    )
    done()
  })

  test('SRV record (SIP)', done => {
    const output = converter.SRV(
      '_sip._tcp.example.net.',
      86400,
      1,
      10,
      5060,
      'sipserver.example.com'
    )

    expect(typeof output).toBe('string')
    expect(output).toBe(
      '_sip._tcp.example.net. 86400 IN SRV 1 10 5060 sipserver.example.com'
    )
    done()
  })

  test('TXT record (SPF)', done => {
    const output = converter.TXT('@', 86400, 'v=spf1 mx -all')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 86400 IN TXT ("v=spf1 mx -all")')
    done()
  })

  test('TXT record (long DMARC signature)', done => {
    const output = converter.TXT(
      'mail._domainkey',
      null,
      'v=DKIM1; k=rsa; s=email; p=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaa+blazzlefrozzleaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaa+aaaaaaaaa'
    )

    expect(typeof output).toBe('string')
    // Tests that our chunking works correctly.
    expect(output).toBe(
      'mail._domainkey IN TXT ("v=DKIM1; k=rsa; s=email; p=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaa+blazzlefrozzleaaaaaaaaaaaaaaaaaaaaaa" "aaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaa+aaaaaaaaa")'
    )
    done()
  })

  test('TXT record (non-ASCII)', done => {
    const output = converter.TXT(
      'food',
      6300,
      'crispy=flæskesvær; greasy=bøfsandwich; common=🍔'
    )

    expect(typeof output).toBe('string')
    expect(output).toBe(
      'food 6300 IN TXT ("crispy=flskesvr; greasy=bfsandwich; common=")'
    )
    done()
  })

  test('TXT record (backslash escape)', done => {
    const output = converter.TXT('backslash', 4930, 'test\\')

    expect(typeof output).toBe('string')
    expect(output).toBe('backslash 4930 IN TXT ("test\\\\")')
    done()
  })

  test('TXT record (backtick escape)', done => {
    const output = converter.TXT('backtick', 1444, 'new`test')

    expect(typeof output).toBe('string')
    expect(output).toBe('backtick 1444 IN TXT ("new``test")')
    done()
  })

  test('TXT record (quote escape)', done => {
    const output = converter.TXT('quote', 1984, 'new"test')

    expect(typeof output).toBe('string')
    expect(output).toBe('quote 1984 IN TXT ("new\\"test")')
    done()
  })

  test('TXT record formatter (empty input)', () => {
    expect(converter.formatTXTContent('')).toBe('')
  })
})

describe('GenericZoneFile recordsTypeDispatch rendering', () => {
  const converter = new GenericZoneFileRecordConverter()

  test('a TXT record (example.net)', done => {
    const record: RecordTypeTXT = {
      prefix: 'test',
      record_type: 'TXT',
      text_content: 'this is a test',
      ttl: 360
    }
    const output = converter.recordTypeDispatch(record)

    expect(typeof output).toBe('string')
    expect(output).toBe('test 360 IN TXT ("this is a test")')
    done()
  })

  test('an invalid record type', done => {
    const record = ({
      prefix: 'test',
      record_type: 'BALJEPIS',
      text_content: 'this is a test',
      ttl: 360
    } as unknown) as RecordTypeTXT

    expect(() => {
      converter.recordTypeDispatch(record)
    }).toThrowError(/^EDZIFCONVERR-001/)

    done()
  })
})

describe('GenericZoneFile records conversion (with absolutifyHostnames option)', () => {
  const converter = new GenericZoneFileRecordConverter({
    absolutifyHostnames: true
  })

  test('CNAME record (empty hostname)', done => {
    const output = converter.CNAME('', 43200, '')

    expect(typeof output).toBe('string')
    expect(output).toBe('@ 43200 CNAME .')
    done()
  })

  test('CNAME record (www to self)', done => {
    const output = converter.CNAME('www', 43200, '@')

    expect(typeof output).toBe('string')
    expect(output).toBe('www 43200 CNAME @')
    done()
  })

  test('CNAME record (www)', done => {
    const output = converter.CNAME('www', 43200, 'example.net')

    expect(typeof output).toBe('string')
    expect(output).toBe('www 43200 CNAME example.net.')
    done()
  })
})
